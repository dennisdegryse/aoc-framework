# AoC Framework

This is a minimal framework I use for [Advent of Code](https://adventofcode.com). It has a CLI tool for automating the following tasks:
* Create new source files
* Edit input files
* Compile and run the files (auto-fetches the online input file for your account)

## Prerequisites

The framework assumes a Linux environment with Bash. See the contents of the `<language>.env` files for any tools that are used for compiling/wrapping the source files.

### Dependencies on Ubuntu 20.04 (LTS)

The following table shows the dependencies for Ubuntu 20.04:

| Package          | Reason                                                       |
|------------------|--------------------------------------------------------------|
| curl             |  fetching puzzles and puzzle input files, submitting answers |
| w3m              |  viewing puzzles                                             |
| haskell-platform |  Compiling haskell sources                                   |
| dotnet-sdk-6.0   |  Compiling and running c# sources                            |
| pypy3            |  Compiling and running python3 sources (optimized)           |
| build-essential  |  Compiling C sources                                         |
| vim              |  Default editor                                              |

## Installation

1. Put the `/src` folder at your preferred location and add it to your `PATH` environment variable.
2. Rename the `.env.example` file to `.env` and set the parameters according to your own preferences. Usually you only need to change the `YEAR` variable.
3. Add/tweak `<language>.env` files to your own need (Bash syntax). The `compile` function inside assumes that all required dependencies are already installed on your system.
4. Run `aoc help` to display the syntax of the CLI tool.

## Session access token

While using the CLI tool, make sure you set the session token. You can peek it from your browser inspector and use the appropriate command to store it. This token expires at some point, so you'll have to update it from time to time. Do **not** share this token with anyone.

The token is used for fetching your personal input file.

## Example workflow

This is an example workflow for viewing, testing and solving a puzzle (including submitting your answer).

First, view the puzzle of the day.

```bash
$ aoc browse 4
```

This will show a scrollable text version of the original puzzle page in your terminal:

```
--- Day 4: Giant Squid ---

You're already almost 1.5km (almost a mile) below the surface of the ocean,
already so deep that you can't see any sunlight. What you can see, however, is
a giant squid that has attached itself to the outside of your submarine.

Maybe it wants to play bingo?

Bingo is played on a set of boards each consisting of a 5x5 grid of numbers.
Numbers are chosen at random, and the chosen number is marked on all boards on
which it appears. (Numbers may not appear on all boards.) If all numbers in any
row or any column of a board are marked, that board wins. (Diagonals don't
count.)

The submarine has a bingo subsystem to help passengers (currently, you and the
giant squid) pass the time. It automatically generates a random order in which
to draw numbers and a random set of boards (your puzzle input). For example:

7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
:
```

Create a test input file based on the information in the puzzle
```bash
$ aoc input 4 test
```

This will open an empty editor in which you can type or paste example puzzle input: 

```vim
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
~
~
~
~
~
~
~
~
"day4/input/test" 19L, 299C
```

Now, create/edit your solution for the puzzle.

```bash
$ aoc edit 4 1 haskell
```

The first time you run this command, the tool will generate a template source file. This template is based on the language you select. After generation it will open the file in your configured editor.

```vim
import System.Clock

type Answer = Int

{- Part 1 -}

solve1 :: a -> Answer
solve1 puzzle = 0

{- Part 2 -}

solve2 :: a -> Answer
solve2 puzzle = 0

{- Input Parsing -}

parse :: [String] -> a
parse lines = lines

{- IO -}

main = do
  raw <- fmap lines getContents

  tstart <- getTime ProcessCPUTime
  let puzzle = parse raw
  print $ solve1 puzzle
  print $ solve2 puzzle
  tstop <- getTime ProcessCPUTime

  print $ toNanoSecs $ diffTimeSpec tstart tstop
~
~
"day4/haskell/part1.hs" 31L, 466C
```

Add the necessary logic to the source file. Then, run your solution against the test input file and check if the output matches.

```bash
$ aoc solve 4 1 haskell test

[1 of 1] Compiling Main             ( run.hs, run.o )
Linking run ...
Part 1: 4512
Part 2: 1924
Time  : 363742 ns
```

If all is fine, run your solution against the automatically fetched personal input file.

```bash
$ aoc solve 4 1 haskell
Downloading puzzle input...
[1 of 1] Compiling Main             ( run.hs, run.o )
Linking run ...
Part 1: 41503
Part 2: 3178
Time  : 10114148 ns
```

When confident, submit the results to Advent of Code and await the result.

```bash
$ aoc submit 4 2
Correct!
```
