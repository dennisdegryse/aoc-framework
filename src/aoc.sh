#!/bin/bash

function fail() {
    >&2 echo "$1"
    exit 1
}

function usage() {
    echo "AoC Automation CL - (c) 2021 Dennis Degryse <dennisdegryse@gmail.com>"
    echo ""
    echo "Usage:"
    echo "   aoc COMMAND [ARGS]"
    echo ""
    echo "Commands:"
    echo ""
    echo "   browse DAY"
    echo "      -> browses the puzzle for the given DAY."
    echo ""
    echo "   solve DAY PART [LANG=${DEFAULT_LANG} [INPUT=${PUZZLE_INPUT}]]"
    echo "      -> runs the solver for the given DAY, PART and LANGUAGE against the INPUT."
    echo ""
    echo "   submit DAY PART [ANSWER=FROM-OUTPUT]"
    echo "      -> submits the given solution and returns the result. If no ANSWER is"
    echo "         specified, the last output for the given DAY and PART is consulted."
    echo ""
    echo "   edit DAY PART [LANG=${DEFAULT_LANG}]"
    echo "      -> edits the solver source for the given DAY, PART and LANG."
    echo ""
    echo "   fork DAY PART [LANG=${DEFAULT_LANG}]"
    echo "      -> forks and edits the solver source for the given DAY, PART and LANG."
    echo ""
    echo "   input DAY [INPUT=${PUZZLE_INPUT}]"
    echo "      -> edits the INPUT for the given DAY."
    echo ""
    echo "   board [NAME=${DEFAULT_BOARD}]"
    echo "      -> shows the board with the given NAME."
    echo ""
    echo "   set-token SESSION_TOKEN"
    echo "      -> stores the SESSION_TOKEN."
    echo ""
    echo "   del-token"
    echo "      -> removes any stored session token."
    echo ""
    echo "   help"
    echo "      -> displays this information."
    echo ""
    echo "Licence: Creative Commons Attribution-ShareAlike 4.0 International"
    echo ""
}

function initEnv() {
    YEAR=${1:-$(date +%Y)}
    cat << EOF > "${GLOBAL_ENV}"
YEAR=${YEAR}
WORKSPACE=~/aoc
PUZZLE_INPUT=puzzle
DEFAULT_LANG=haskell
SESSION_TOKEN_FILE=~/.aocrc
INVALID_TOKEN_FEEDBACK="Puzzle inputs differ by user.  Please log in to get your puzzle input."
PUZZLE_URI="https://adventofcode.com/\${YEAR}/day/\${2}/input"
EDITOR=vim
EOF
}

function getToken() {
    if ! [ -f "${SESSION_TOKEN_FILE}" ]; then
        fail "Session token file for AoC was not found. Please run aoc set-token <token>."
    fi

    cat ${SESSION_TOKEN_FILE}
}

function browse() {
    TOKEN=$(getToken)

    curl -s "https://adventofcode.com/${YEAR}/day/${DAY}" -b "session=${TOKEN}" \
      | w3m -dump -T "text/html" \
      | sed -n '/^--- Day/,$p' \
      | less
}

function fetchInput() {
    TARGET="$1"
    TOKEN=$(getToken)

    if ! [ -f "${SESSION_TOKEN_FILE}" ]; then
        fail "Session token file for AoC was not found. Please run aoc set-token <token>."
    fi

    echo "Downloading puzzle input..."
    curl "${PUZZLE_URI}" \
     -s \
         --header "cookie: session=${TOKEN}" \
         --output "${TARGET}"

    if [[ "$(cat ${TARGET})" == "${INVALID_TOKEN_FEEDBACK}" ]]; then
        rm -f "${TARGET}"
        fail "Session token is no longer valid. Please run aoc set-token <token>."
    fi
}

function loadInput() {
    export INPUT=${1:-${PUZZLE_INPUT}}
    export INPUT_DIR="day${DAY}/input"
    export INPUT_FILE="${INPUT_DIR}/${INPUT}"
    OPTIONAL=${2}

    if ! [ -f "${INPUT_FILE}" ]; then
        mkdir -p "${INPUT_DIR}"

        if [[ "${INPUT}" == "${PUZZLE_INPUT}" ]]; then
            fetchInput "${INPUT_FILE}"
        else
            if [[ "${OPTIONAL}" != "optional" ]]; then
                fail "Requested custom puzzle input could not be found."
            fi
        fi
    fi
}

function submitAnswer() {
    PART="${1}"
    ANSWER="${2}"
    TOKEN=$(getToken)

    curl -X POST "https://adventofcode.com/${YEAR}/day/${DAY}/answer" \
         -s \
         -i \
         -H "Content-Type: application/x-www-form-urlencoded" \
         -b "session=${TOKEN}" \
         -d "level=${PART}&answer=${ANSWER}" \
      | sed -n '/<article>/,${p;/<\/article>/q}' - \
      | sed -E 's/^.*([0-9][0-9]*m [0-9][0-9]*s|not the right|already complete|right answer!).*$/\1/g' -
}

GLOBAL_ENV="${HOME}/.aoc.env"

if ! [ -f "${GLOBAL_ENV}" ]; then
    if [[ "$1" != "init" ]]; then
        fail "No AoC environment was found. Run aoc init to initialize an environment for the current user."
    fi
else
    set -a
    . ~/.aoc.env
    set +a
fi

cd "${WORKSPACE}/${YEAR}"

echo "Working in ${WORKSPACE}/${YEAR}"

case "$1" in
help)
    usage
    ;;
init)
    if [ -f "${GLOBAL_ENV}" ]; then
        printf "An environment already exists for the current user. Do you want to replace it? [y/N] "
        read ANS

        if [[ "${ANS}" == "y" ]]; then
             rm -f "${LOBAL_ENV}"
        else
             exit 0
        fi
    fi

    initEnv "$2"
    ;;
set-token)
    if [[ "$2" == "" ]]; then
        fail "Session token not supplied."
    fi

    echo "$2" > "${SESSION_TOKEN_FILE}"
    echo "Session was stored in ${SESSION_TOKEN_FILE}."
    ;;
del-token)
    rm -f "${SESSION_TOKEN_FILE}"
    echo "Session token was deleted."
    ;;
board)
    NAME="${2:-${DEFAULT_BOARD}}"
    ;;
browse)
    DAY=${2}

    browse "${DAY}"
    ;;
input)
    DAY=${2}

    loadInput "$3" optional
    ${EDITOR} "${INPUT_FILE}"
    ;;
submit)
    DAY=${2}
    PART=${3}
    ANSWER=${4:-FROM-OUTPUT}

    if [[ "${ANSWER}" == "FROM-OUTPUT" ]]; then
        ANSWER=$(cat runtime/output-${DAY}.txt 2>/dev/null | sed "${PART}q;d" -)
    fi

    if [[ "${ANSWER}" == "" ]]; then
        fail "No solution was given and no output file exists for day ${DAY}."
    fi

    RESULT=$(submitAnswer ${PART} "${ANSWER}")

    case "${RESULT}" in
    'right answer!')
        echo "Correct!"
        ;;

    'not the right')
        echo "Wrong!"
        ;;

    'already complete')
        echo "Already completed..."
        ;;
    *)
        echo "Retry in ${RESULT}..."
        ;;
    esac
    ;;
solve|edit|fork|debug)
    if ! [[ "$2" =~ ^([1-9]|1[0-9]|2[0-5])$ ]]; then
        fail "Day not supplied or invalid: expected 1..25."
    fi

    if ! [[ "$3" =~ ^(1|2)$ ]]; then
        fail "Part not supplied or invalid: expected 1 or 2."
    fi

    DAY=${2}
    PART=${3}
    LANG=${4:-python}

    set -a
    . ${LANG}.env
    set +a

    LANGDIR="day${DAY}/${LANG}"
    SOURCE_FILE="${LANGDIR}/part${PART}.${EXT}"

    case "$1" in
    fork)
        cp "${SOURCE_FILE}" "${SOURCE_FILE}.bak-$(uuidgen)"
        ${EDITOR} "${SOURCE_FILE}"
        ;;
    edit)
        if ! [ -f "${SOURCE_FILE}" ]; then
            mkdir -p "${LANGDIR}"
            echo "$TEMPLATE" > "${SOURCE_FILE}"
        fi
        ${EDITOR} "${SOURCE_FILE}"
        ;;
    debug|solve)
        loadInput "$5"

        rm -Rf runtime
        mkdir runtime

        if [ -f "${SOURCE_FILE}" ]; then
            ln -s "../${SOURCE_FILE}" runtime/run.${EXT}
        fi

        cd runtime
        compile run.${EXT}

        case "$1" in
        solve)
            cat "../${INPUT_FILE}" | ./run > "output-${DAY}.txt"
            readarray -t RESULT < "output-${DAY}.txt"
            COUNT="${#RESULT[@]}"

            if [[ "${#RESULT[@]}" == "3" ]]; then
                echo "Part 1: ${RESULT[0]}"
                echo "Part 2: ${RESULT[1]}"
                echo "Time  : ${RESULT[2]} ns"
            else
                fail "$OUTPUT"
            fi
            ;;
        debug)
            cat "../${INPUT_FILE}" | ./run
        esac
        ;;
    esac
    ;;
*)
    fail "Invalid subcommand. Run aoc help for more information."
    ;;
esac

exit 0

exit 0
